# strangequark.tk
This is a mirror of my website at http://strangequark.tk

You can use it to create your own website/blog.


##How to install it
You just need to put the files on your server, you need a WebServer (I use Apache) and PHP5.

You need to change the variable $base_url in index.php to match your site url.

The static directory is where you put your static pages, you can access this pages by going to /index.php/pagename

The blog articles are located in article directory, they are written in [Markdown](http://daringfireball.net/projects/markdown/syntax), the conversion is done by [Parsedown](https://github.com/erusev/parsedown) (already included). The article list is automatically generated, you can see it in index.php/blog.

##Writing articles
You need to start the article this way:
```
[!TITLE]My article title
[!DATE]Sun, 05 Jul 2015 13:20:00 +0000
[!END]
```
*The +0000 indicates that this is the time in GMT0

The rest of the article must be written using [markdown](http://daringfireball.net/projects/markdown/syntax).
