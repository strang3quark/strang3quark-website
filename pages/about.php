<p>My name is <a href="http://brunojesus.tk" class="text-marker-pink"> Bruno Jesus, strang3quark</a>, 
I'm a developer currently working for <a href="http://maxdata.pt" class="text-marker-orange">Maxdata Software</a> on the Clinidata Project (a healthcare solution).</p>
<p>I am Portuguese so English isn't my native language, if there are any typos or you don't understand
something I wrote please <a href="<?=BASE_URL;?>index.php/contact" class="text-marker-blue">e-mail me</a>.</p>
<p>I like Linux and the OpenSource philosophy, I don't consider myself a fanboy, but I use free software when possible.</p>
<p>You may notice that this site is really "minimal", this is how I like things, nice and simple.</p>
<p>One of the things I like the most is programming, you can check my <a href="https://gitlab.com/strang3quark" class="text-marker-black-blue">GitLab</a>. This website code is available <a href="https://gitlab.com/strang3quark/strang3quark-website" class="text-marker-red">here</a>.</p>
<p>That's all, I hope you enjoy the <a href="<?=BASE_URL;?>index.php/blog" class="text-marker-orange">blog</a>, if you have
some suggestions feel free to <a href="<?=BASE_URL;?>index.php/contact" class="text-marker-blue">contact me</a>.</p>
