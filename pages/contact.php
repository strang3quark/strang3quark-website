<p><b>Email:</b> <code>echo fgenat3dhnexNGevfrhcQBGarg | tr a-zA-Z n-za-mN-ZA-M</code></p>
<p>If you are paranoid, use <a href="http://keys.gnupg.net/pks/lookup?op=get&search=0xB23DB96D381A2B18">this key</a>.
</p>

<br>
<b>Places you can find me online:</b>
<ul>
	<li>irc.freenode.com - Mostly in #archlinux</li>
	<li>irc.nixers.net - #unix</li>
	<li><a href="https://www.reddit.com/user/strang3quark">reddit.com</a> - Mostly in /r/unixporn and /r/archlinux</li>
	<li><a href="http://nixers.net/member.php?action=profile&uid=1303">nixers.net</a></li>
</ul>
