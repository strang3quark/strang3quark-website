<h1><a href="#">Articles</a> — (<a href="<?=BASE_URL;?>index.php/feed">RSS Feed</a>)</h1>
<ul>
<?php $articleList = $blog->getArticleList();
foreach ($articleList as $articleFile):
	$header = $blog->getHeader($blog->getArticle($articleFile));
	$title = $header[0];
	$date = explode('_', $articleFile)[0];
	$link = $blog->getArticleLink($articleFile);
?>
	<li><?=$date;?> - <a href="<?=$link;?>"><?=$title;?></a></li>
<?php endforeach;?>
</ul>
