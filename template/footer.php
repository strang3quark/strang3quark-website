		</section>
	<footer id="footer">
			<nav class="navigation">
				<ul>
					<li><a href="http://brunojesus.tk"><i class="fa fa-id-card" aria-hidden="true"></i> Portfolio/CV</a></li>
					<li><a href="https://gitlab.com/strang3quark/strang3quark-website"><i class="fa fa-code" aria-hidden="true"></i> SourceCode</a></li>
					<li><a href="https://gitlab.com/strang3quark"><i class="fa fa-gitlab" aria-hidden="true"></i> GitLab</a></li>
					<?php if (isset($numVisits)): ?>
						<li id="visitNum"><i class="fa fa-eye" aria-hidden="true"></i> <?=$numVisits;?></li>
						<div style="clear: both;"></div>
					<?php endif; ?>
				</ul>
			</nav>			
	</footer>
	</div>
</body>
</html>
