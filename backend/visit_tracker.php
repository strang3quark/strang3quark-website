<?php
class VisitTracker {
    
    function __construct($document){
        $this->path = $document;
        $this->initializeXMLFile($document);
    }

    function addVisit($page, $article){
        $nrVisits = 1;
        $nrVisitsNode = $this->query('//visits/'.$page.'[@article="'.$article.'"]/total');

        if ($nrVisitsNode->length == 0){
            $this->createPage($page,$article);
        }else{
            $node = $nrVisitsNode->item(0);
            $node->nodeValue = ++$node->nodeValue;
            $nrVisits = $node->nodeValue;
            $this->save();
        }

        return $nrVisits;
    }

    private function save(){
        $this->xml->save($this->path);
    }

    private function initializeXMLFile($path){
        if (!file_exists($path)){
            $newXml = new DOMDocument();
            $rootElem = $newXml->createElement('visits');
            $newXml->appendChild($rootElem);
            $newXml->save($path);
            $newXml = null;
        }

        $this->xml = new DOMDocument();
        $this->xml->load($path);
    }

    private function query($xpathExp){
        $xpath = new DomXpath($this->xml);
        return $xpath->query($xpathExp);
    }

    private function createPage($page,$article){
        $xmlRoot = $this->query('//visits')->item(0);

        $pageElem = $this->xml->createElement($page);
        $pageElem->setAttribute('article',$article);
        $totalElem = $this->xml->createElement('total',1);

        $xmlRoot->appendChild($pageElem);
        $pageElem->appendChild($totalElem);

        $this->save();
    }
}