<?php
/**
 * Website configuration
 *
 * @author Bruno Jesus (aka strang3quark) <bruno.fl.jesus@gmail.com>
 * @version 0.1
 * @copyright (C) 2016 Bruno Jesus (aka strang3quark) <bruno.fl.jesus@gmail.com>
 * @license MIT
 */

define('BASE_URL',Utils::baseUrl());
define('DEFAULT_PAGE','about');
define('SITE_TITLE','Quark_2.0'); //the <title> tag content
define('SITE_DESCRIPTION', 'Some wannabe programmer blog');
define('HEADER_TITLE','Quark_2.0'); //the header page title
