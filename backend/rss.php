<?php
class RSS{
	function __construct($blog){
		$this->blog = $blog;	
	}
	function getFeed($nrArticles = 10){
		$header = 
'<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/">
	<channel>
		<title>'.HEADER_TITLE.'</title>
		<link>'.BASE_URL.'</link>
		<description>'.SITE_DESCRIPTION.'</description>';		
		$body = $this->getArticles($nrArticles);
		$footer = '
	</channel>
</rss>';

		return $header.$body.$footer;
	}

	function getArticles($nrArticles = 10){
		$data = "";
		$articleFiles = $this->blog->getArticleList();

		if ($nrArticles > sizeof($articleFiles)){
			$nrArticles = sizeof($articleFiles);	
		}

		for ($i=0; $i < $nrArticles; $i++){
			$rawcontent = $this->blog->getArticle($articleFiles[$i]);
			$metadata = $this->blog->getHeader($rawcontent);

			$htmlcontent = $this->blog->markdownToHtml($this->blog->getBodyMarkdown($rawcontent));
			$data .= '
				<item>
					<title>'.$metadata[0].'</title>
					<link>'.$this->blog->getArticleLink($articleFiles[$i]).'</link>
					<pubDate>'.$metadata[1].'</pubDate>
					<content:encoded><![CDATA['.$htmlcontent.']]></content:encoded>
				</item>';
		}
		return $data;
	}

}



?>
