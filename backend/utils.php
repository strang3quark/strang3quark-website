<?php
class Utils {
    public static function baseUrl(){
        $url = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );

        //drop index.php
        return substr($url,0,stripos($url,'/index.php'))."/";
    }

    public static function currentDateAsInteger(){
        return intval(date("Ymd"));
    }
}