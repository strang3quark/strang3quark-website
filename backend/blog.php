<?php
class Blog{
	function __construct($base_url = ''){
		$this->base_url = $base_url;
		$this->parser = new Parsedown();
		$this->articles_path = realpath('./articles/').'/';
	}

	function getArticleList(){
		$scanned_directory = scandir($this->articles_path);
		//the index starts at 2, so we need to create a 0-based array //and in reverse order
		$result = array();
		for ($i=sizeof($scanned_directory) - 1; $i > 1; $i--){
			if ($scanned_directory[$i] != "temp.txt") {
				array_push($result, $scanned_directory[$i]);
			}
		}
		return $result;
	}

	function getArticleLink($articlefile){
		$article = substr( $articlefile, 0, strpos($articlefile, '.') );
		return $this->base_url.'index.php/blog/'.$article;		
	}

	function getHeader($text)
	{        
		/*Gets the title and date - usefull for RSS*/
		$raw = substr( $text, 0, strpos($text, "[!END]") );
		$title = substr( $raw, strpos($text, "[!TITLE]")+8, strpos($text, "[!DATE]")-strpos($text, "[!TITLE]")-8 );
		$date = rtrim(substr( $raw, strpos($text, "[!DATE]")+7 ));
		
		//remove newlines
		$title = trim(preg_replace('/\s\s+/', ' ', $title));
		$date = trim(preg_replace('/\s\s+/', ' ', $date));
		return array($title, $date);
	}

	function getBodyMarkdown($text){
		/*Gets the article body*/
		$text = explode("(#) &mdash;",$text)[1];
		return substr( $text, strpos($text, "\n") + 1 );
	}

	/* Gets the file content */
	function getArticle($article){
		if (strpos($article,'.txt') > -1){
			$article = substr($article,0,strpos($article,'.txt')); //remove .txt if we already have it
		}

		if (file_exists($this->articles_path.$article.'.txt') === false){
			return false;
		}

		return file_get_contents($this->articles_path.$article.'.txt');
	}

	function markdownToHtml($markdown){
		return $this->parser->text($markdown);
	}

	function getArticleHTML($articleContent){
		if ($articleContent === false){ //file not found
			return false;
		}

		$text = explode("[!END]", $articleContent);
		if (sizeof($text) == 1){ #if article does not have header ignore
			$text = $text[0];
		}else{ #has header, get the second part
			$text = $text[1];
		}
		$htmlcontent = $this->markdownToHtml($text);
		return $htmlcontent;
	}
}
?>
