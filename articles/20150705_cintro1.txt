﻿[!TITLE]Programming in C (Part 1)
[!DATE]Sun, 05 Jul 2015 13:20:00 +0000
[!END]

# [Programming in C (Part 1)](#) &mdash; 05 July, 2015

Hi guys, this is the beginning of a series of articles about Programming in C.
The goal of this series is to teach programming for everyone willing to learn, while teaching you I am also learning, I have some experience in other languages but I never did anything "complex" with C++.

If you spot a mistake in any of this articles please contact-me, I'm learning with this too, so it's possible that I'll make some mistakes along the way.

## What do you need to follow this series
The most important thing that you need is willingness to learn, without that you won't learn nothing here.

You can use any operating system you want, but I recommend using Linux, any distribution will work for you, use the one that you prefer.

There's no need for an IDE, a simple notepad is enough, I'm using vim but like the operating system you can use the any text editor, if you don't know which one to use you have Atom or Sublime Text or even gedit.

You need the g++ compiler, you can get that by installing <code>build-essential</code> in Ubuntu or `base-devel` in Arch.

## What's a program?
A program is just a list of instruction that produce a result once executed.

You can think about a program as a recipe, you have the ingredients and depending on the way you mix them you will have tasteful cake, do it in the wrong way and it may be a disaster (You can't put it in the furnace before mixing the ingredients).

In this series we will cook some recipes and you can analyse and change them to suit your taste.

If I said to you to change a light bulb it should be easy for you, unfortunately the computers are dumb so you have to tell everything step-by-step, example:

- Move the hand towards the light bulb
- Grab the light bulb
- Twist it to the left side until it's free
- Lift the light bulb
- Drop it in the table
- Grab the new light bulb
- Put it in socket
- Twist it to the right until it's fitted
- Done

It may seem complicated but once you get it, it will all come naturally.

That's all for today, hope you enjoy it.
